﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using CodeChallenge.AGL.Models;
using CodeChallenge.AGL.Transforms;
using NUnit.Framework;
using Should;
using SpecsFor;

namespace CodeChallenge.AGL.Tests
{
    public class PetsUnderGendersTransformSpecs
    {
        public class Given_No_PetOwners_When_Applying_Transform : SpecsFor<PetsUnderGendersTransform>
        {
            protected IEnumerable<PetOwner> PetOwners;
            protected IEnumerable<PetsUnderGender> PetsUnderGenders;

            protected override void Given()
            {
                base.Given();

                PetOwners = Enumerable.Empty<PetOwner>();
            }

            protected override void When()
            {
                PetsUnderGenders = SUT.Apply(PetOwners);
            }

            [Test]
            public void PetsUnderGenders_Is_Empty()
            {
                PetsUnderGenders.ShouldBeEmpty();
            }
        }

        public class Given_PetOwners_When_Applying_Transform : SpecsFor<PetsUnderGendersTransform>
        {
            protected IEnumerable<PetOwner> PetOwners;
            protected IEnumerable<PetsUnderGender> PetsUnderGenders;

            protected override void Given()
            {
                base.Given();

                PetOwners = new[]
                {
                    new PetOwner
                    {
                        Name = "Bob",
                        Gender = Gender.Male,
                        Pets =
                            new[]
                            {
                                new Pet {Name = "Garfield", Type = PetType.Cat},
                                new Pet {Name = "Fido", Type = PetType.Dog}
                            }
                    },

                    new PetOwner
                    {
                        Name = "Jennifer",
                        Gender = Gender.Female,
                        Pets = new[] {new Pet {Name = "Garfield", Type = PetType.Cat}}
                    },

                    new PetOwner {Name = "Steve", Gender = Gender.Male, Pets = null},

                    new PetOwner
                    {
                        Name = "Fred",
                        Gender = Gender.Male,
                        Pets =
                            new[]
                            {
                                new Pet {Name = "Tom", Type = PetType.Cat}, new Pet {Name = "Max", Type = PetType.Cat},
                                new Pet {Name = "Same", Type = PetType.Dog}, new Pet {Name = "Jim", Type = PetType.Cat}
                            }
                    },

                    new PetOwner
                    {
                        Name = "Samantha",
                        Gender = Gender.Female,
                        Pets = new[] {new Pet {Name = "Tabby", Type = PetType.Cat}}
                    },

                    new PetOwner
                    {
                        Name = "Alice",
                        Gender = Gender.Female,
                        Pets =
                            new[]
                            {new Pet {Name = "Simba", Type = PetType.Cat}, new Pet {Name = "Nemo", Type = PetType.Fish}}
                    }
                };
            }

            protected override void When()
            {
                PetsUnderGenders = SUT.Apply(PetOwners);
            }

            [Test]
            public void PetsUnderGenders_Is_Not_Empty()
            {
                PetsUnderGenders.ShouldNotBeEmpty();
            }

            [Test]
            public void PetsUnderGenders_Has_A_List_Of_Cats_OrderBy_Name_Under_Male()
            {
                PetsUnderGenders.SingleOrDefault(petsUnderGender => petsUnderGender.Gender == Gender.Male).ShouldNotBeNull();

                var pets = PetsUnderGenders.Single(petsUnderGender => petsUnderGender.Gender == Gender.Male).Pets.ToList();

                pets.Count().ShouldEqual(4);
                pets.First().Name.ShouldEqual("Garfield");
                pets.ElementAt(1).Name.ShouldEqual("Jim");
                pets.ElementAt(2).Name.ShouldEqual("Max");
                pets.Last().Name.ShouldEqual("Tom");
            }

            [Test]
            public void PetsUnderGenders_Has_A_List_Of_Cats_OrderBy_Name_Under_Female()
            {
                PetsUnderGenders.SingleOrDefault(petsUnderGender => petsUnderGender.Gender == Gender.Female).ShouldNotBeNull();

                var pets = PetsUnderGenders.Single(petsUnderGender => petsUnderGender.Gender == Gender.Female).Pets;

                pets.Count().ShouldEqual(3);
                pets.First().Name.ShouldEqual("Garfield");
                pets.ElementAt(1).Name.ShouldEqual("Simba");
                pets.ElementAt(2).Name.ShouldEqual("Tabby");
            }
        }
    }
}
