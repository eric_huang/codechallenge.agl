﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using CodeChallenge.AGL.Models;
using Newtonsoft.Json;

namespace CodeChallenge.AGL.Repos
{
    public class PetOwnersRepository : IPetOwnersRepository
    {
        public async Task<IEnumerable<PetOwner>> GetPetOwners()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://agl-developer-test.azurewebsites.net");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = await client.GetAsync("people.json");

                if (!response.IsSuccessStatusCode)
                {
                    throw new ApplicationException(string.Format("Failed to get people.json. StatusCode is {0}", response.StatusCode));
                }

                return await response.Content.ReadAsAsync<IEnumerable<PetOwner>>(GetHttpClientJsonMediaTypeFormatters());
            }
        }

        private IEnumerable<JsonMediaTypeFormatter> GetHttpClientJsonMediaTypeFormatters()
        {
            // To deal with null collection
            return new[]
            {
                new JsonMediaTypeFormatter
                {
                    SerializerSettings = new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore
                    }
                }
            };
        }
    }
}
