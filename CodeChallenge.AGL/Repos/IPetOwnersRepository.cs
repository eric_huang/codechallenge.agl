﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CodeChallenge.AGL.Models;

namespace CodeChallenge.AGL.Repos
{
    /// <summary>
    /// Repo for PetOwners
    /// </summary>
    public interface IPetOwnersRepository
    {
        /// <summary>
        /// Get all pet owners
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<PetOwner>> GetPetOwners();
    }
}
