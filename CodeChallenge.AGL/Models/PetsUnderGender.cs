﻿using System.Collections.Generic;

namespace CodeChallenge.AGL.Models
{
    public class PetsUnderGender
    {
        public Gender Gender { get; private set; }
        public IEnumerable<Pet> Pets { get; private set; }

        public PetsUnderGender(Gender gender, IEnumerable<Pet> pets)
        {
            Gender = gender;
            Pets = pets;
        }
    }
}