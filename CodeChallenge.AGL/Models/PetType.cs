﻿namespace CodeChallenge.AGL.Models
{
    public enum PetType
    {
        Dog,
        Cat,
        Fish
    }
}