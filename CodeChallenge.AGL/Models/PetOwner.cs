﻿using System.Collections.Generic;
using System.Linq;

namespace CodeChallenge.AGL.Models
{
    public class PetOwner
    {
        public PetOwner()
        {
            Pets = Enumerable.Empty<Pet>();
        }

        public string Name { get; set; }
        public Gender Gender { get; set; }

        public IEnumerable<Pet> Pets { get; set; }
    }
}