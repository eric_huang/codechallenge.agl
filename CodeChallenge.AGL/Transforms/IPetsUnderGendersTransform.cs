﻿using System.Collections.Generic;
using CodeChallenge.AGL.Models;

namespace CodeChallenge.AGL.Transforms
{
    /// <summary>
    /// A set of transformations to convert pet owners into a different model
    /// </summary>
    public interface IPetsUnderGendersTransform
    {
        /// <summary>
        /// Convert pet owners to PetsUnderGender model 
        /// </summary>
        /// <param name="petOwners">pet owners</param>
        /// <returns></returns>
        IEnumerable<PetsUnderGender> Apply(IEnumerable<PetOwner> petOwners);
    }
}