﻿using System.Collections.Generic;
using System.Linq;
using CodeChallenge.AGL.Models;

namespace CodeChallenge.AGL.Transforms
{
    // There are only two hard things in Computer Science: cache invalidation and naming things.
    //  I hope I name it correct ;)

    public class PetsUnderGendersTransform : IPetsUnderGendersTransform
    {
        public IEnumerable<PetsUnderGender> Apply(IEnumerable<PetOwner> petOwners)
        {
            return petOwners
                .GroupBy(petOwner => petOwner.Gender)
                .Select(generGroup => new PetsUnderGender(
                    generGroup.Key,
                    generGroup.SelectMany(
                        petOwner => petOwner.Pets == null
                            ? Enumerable.Empty<Pet>()
                            : petOwner.Pets.Where(pet => pet.Type == PetType.Cat)).OrderBy(pet => pet.Name)));
        }
    }
}
