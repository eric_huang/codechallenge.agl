﻿using System;
using Autofac;
using CodeChallenge.AGL.Repos;
using CodeChallenge.AGL.Transforms;

namespace CodeChallenge.AGL
{
    class Program
    {
        static void Main(string[] args)
        {
            // Setup IoC / dependencies
            var container = SetUpIoCContainer();

            using (var scope = container.BeginLifetimeScope())
            {
                var petOwnersRepository = scope.Resolve<IPetOwnersRepository>();
                var petsUnderGendersTransform = scope.Resolve<IPetsUnderGendersTransform>();

                var petOwners = petOwnersRepository.GetPetOwners().Result;
                var petsUnderGenders = petsUnderGendersTransform.Apply(petOwners);

                foreach (var petsUnderGender in petsUnderGenders)
                {
                    Console.WriteLine("{0}", petsUnderGender.Gender);

                    foreach (var pet in petsUnderGender.Pets)
                    {
                        Console.WriteLine("\t{0}\t{1}", pet.Name, pet.Type);
                    }
                }
            }

            Console.ReadLine();
        }

        private static IContainer SetUpIoCContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<PetOwnersRepository>().As<IPetOwnersRepository>();
            builder.RegisterType<PetsUnderGendersTransform>().As<IPetsUnderGendersTransform>();
            return builder.Build();
        }
    }
}
